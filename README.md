# WARNING

This is an unofficial update for RWMS to 1.1. The update check has been completely disabled to avoid confusion.
Also just to get any license stuff out of the way: all my changes are done under GPL2 and in case shakeyourbunny approves of my fixes I grant him the full rights to them.

# RWMS - RimWorld Mod Sorter

[Download](https://bitbucket.org/shakeyourbunny/rwms/downloads/?tab=downloads) (includes Windows executable)

#### Links
Discussion thread on Ludeon: https://ludeon.com/forums/index.php?topic=48518.0 

## Description
This Python script sorts your ModConfigs.xml (RimWorld mod configuration) for better loading time 
putting the dependencies of your mod order in the right spot. This is also the only function of the 
script, it will do this one thing and nothing else. 

Managing mods ((un)subscribing, mod load lists etc) is beyond the scope of this script, there 
is a better ingame solution called "Mod Manager" by Fluffy. The only thing it does, is optimizing
the mod load order.

For using the script you need an active internet connection which will connect to Github, where
the current sort order database resides.

[Full documentation here](docs/documentation.md)

## License
Script written by shakeyourbunny <shakeyourbunny@gmail.com> 

RWMS is licensed under the GNU GPL v2.

Note that you are not allowed to take parts of the scripts for using them in your own mod sorting or
mod manager scripts without written permission. 

## Wanna show your appreciation? Buy me a coffee

[![image](https://i.imgur.com/QGcents.png)](https://ko-fi.com/shakeyourbunny)
